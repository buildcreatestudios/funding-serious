<?php global $post; ?>
<?php wp_nonce_field('save_campaign_meta', 'campaign_meta_nonce'); ?>

<div class="fs-meta-fields">
    <h4><?php _e('GENERAL INFORMATION','fs'); ?></h4>
    <p>
        <label for="fs-campaign-start"><?php _e('Campaign Start','fs'); ?></label>
        <input type="text" class="fs-datetime-picker" name="fs_campaign_start" id="fs-campaign-start" value="<?php echo get_post_meta($post->ID, 'fs_campaign_start', true); ?>" />
    </p>      
    <p>
        <label for="fs-campaign-end"><?php _e('Campaign End','fs'); ?></label>
        <input type="text" class="fs-datetime-picker" name="fs_campaign_end" id="fs-campaign-end" value="<?php echo get_post_meta($post->ID, 'fs_campaign_end', true); ?>" />
    </p>      
    <p>
        <label for="fs-campaign-goal"><?php _e('Campaign Goal','fs'); ?></label>
        <input type="number" name="fs_campaign_goal" id="fs-campaign-goal" value="<?php echo get_post_meta($post->ID, 'fs_campaign_goal', true); ?>" />
    </p>      
    <p>
        <label for="fs-other-contributions"><?php _e('Other Contributions','fs'); ?></label>
        <input type="number" name="fs_other_contributions" id="fs-other-contributions" value="<?php echo get_post_meta($post->ID, 'fs_other_contributions', true); ?>" />
    </p>   
    <br/>
    
    <h4><?php _e('GRAVITY FORM MAPPING','fs'); ?></h4>
    <p>
        <?php $forms = RGFormsModel::get_forms(null, 'title'); ?>
        <?php if($forms) : ?>
			<?php $gf_id = get_post_meta($post->ID, 'fs_gravity_form',true); ?>
            <label for="fs-gravity-form"><?php _e('This Campaign\'s Gravity form','fs'); ?></label> 
            <select id="fs-gravity-form" name="fs_gravity_form" class="fs-gravity-forms">
                <?php foreach( $forms as $form ): ?>
                    <option value="<?php echo $form->id; ?>"
                        <?php 
                            if(empty($gf_id)){
                                if($form->id == $this->fs_settings['default_campaign_form_id']){echo ' selected="selected"';}
                            }else{
                                if($form->id == $gf_id){echo ' selected="selected"';} 
                            }   
                        ?>
                    ><?php echo $form->title; ?></option>
                <?php endforeach; ?>
            </select>
        <?php else : ?>
            <?php printf(__('No Gravity forms have been created yet. %1$sPlease create one here first%2$s','fs'),'<a href="/wp-admin/admin.php?page=gf_new_form">','</a>'); ?>
        <?php endif; ?>
    </p>
    <div class="fs-gf-form-fields"></div>
    <br/>

    <h4><?php _e('CONTRIBUTOR DISPLAY SETTINGS','fs'); ?></h4>
    <p>
        <label><?php _e('Honor Roll Listing','fs'); ?></label>
        <?php $options = array('top' => 'Top Contributors', 'recent' => 'Recent Contributors', 'current' => 'Current contributors (random)', 'none' => 'Do not list any contributors'); ?>
        <select name="fs_honor_roll_listing">
            <?php $honor_roll_listing = get_post_meta($post->ID,'fs_honor_roll_listing',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($honor_roll_listing == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <?php $num_contributors = get_post_meta($post->ID, 'fs_number_of_contributors', true); ?>
        <label for="fs-number-of-contributors"><?php _e('Maximum number of contributors to display','fs'); ?></label>
        <input type="number" name="fs_number_of_contributors" id="fs-number-of-contributors" value="<?php if($num_contributors){echo $num_contributors;}else{echo '10';} ?>" />
    </p>  
     
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.fs-datetime-picker').datetimepicker({
                format:'Y-m-d H:i',
                formatTime:'g:i a', 
                step: 30,
            });

            // ajax call to populate form fields for selected gravity form
            $('.fs-gravity-forms').on('change',function(){
                var form_id = $(this).val();
                var data = {
                    'action': 'fs_get_gravity_form_fields',
                    'form_id': form_id,
                    'post_id': <?php echo $post->ID; ?>
                };
                $.post(ajaxurl, data, function(response){
                    $('.fs-gf-form-fields').html(response); 
                }); 
            }).change();
        });
    </script>    
</div>