<div class="fs-main-wrap">
	<h1 class="fs-page-title"><i class="icon-dollar"></i> Funding Serious - Settings</h1>
	<div id="fs-settings-wrap">
		<form method="post" action="">
			<p>
				<button class="button" name="fs_import_default_campaign_form" value="1">Import Funding Serious Campaign Forms <i class="icon-angle-right"></i></button>
			</p>
			<br/>

			<h3><?php _e('Default Campaign Form','fs'); ?></h3>
			<p>	
				<?php // check for gravity forms first ?>
				<?php if(class_exists('GFForms')) : ?>				
					<?php $forms = RGFormsModel::get_forms(null, 'title'); ?>
			        <?php if($forms) : ?>
						<select name="fs_default_campaign_form_id">
							<option value=""> - Choose One - </option>
						<?php foreach($forms as $form) : ?>
							<option value="<?php echo $form->id; ?>"<?php if($form->id == $this->fs_settings['default_campaign_form_id']){echo ' selected="selected"';} ?>>
								<?php echo $form->title; ?>
							</option>
						<?php endforeach; ?>
						</select>
						<br/><span class="description">This form will be selected in campaigns by default.</span>
			        <?php else : ?>
			            No Gravity forms have been created yet. Please click the button above.
			            <input type="hidden" name="fs_default_campaign_form_id" value="" />
			        <?php endif; ?>
			    <?php endif; ?>
			</p>
			<br/>

			<h3><?php _e('Create Micro Campaign Page','fs'); ?></h3>
			<?php $pages = get_posts('post_type=page&posts_per_page=-1'); ?>
			<?php if($pages) : ?>
			<p>
				Which page is your <code>[fs-micro-campaign-form]</code> shortcode on?<br/>
				<select name="fs_create_micro_campaign_page">
					<option value="">- Choose a Page -</option>
					<?php foreach($pages as $page) : ?>
						<option value="<?php echo $page->ID; ?>" <?php if($this->fs_settings['create_micro_campaign_page'] == $page->ID){echo 'selected="selected"';} ?>><?php echo $page->post_title; ?></option>
					<?php endforeach; ?>
				</select>
				<br/><span class="description">This is necessary to enable user created Micro Campaigns.</span>
			</p>	
			<?php endif; ?>	

			<p>
				<label>Micro Campaign CTA Text:</label>
				<input class="fs-full-width" type="text" name="fs_micro_campaign_cta_text" value="<?php echo $this->fs_settings['micro_campaign_cta_text']; ?>"/>
				<br/><span class="description">This is the CTA button at the bottom of the campaign page sidebar.</span>
			</p>	
			<br/>

			<h3><?php _e('Show Title on Campaign page?','fs'); ?></h3>
			<p>
				<select name="fs_show_campaign_title">
					<option value="yes"<?php if($this->fs_settings['show_campaign_title'] == 'yes'){echo 'selected="selected"';} ?>>Yes</option>
					<option value="no" <?php if($this->fs_settings['show_campaign_title'] == 'no'){echo 'selected="selected"';} ?>>No</option>
				</select>
			</p>	
			<br/>
			
			<h3><?php _e('Campaign Template CSS Selectors','fs'); ?></h3>
			<p>
				<label><?php _e('Content Area Selector','fs'); ?></label>
				<input type="input" name="fs_content_area_selector" value="<?php echo $this->fs_settings['content_area_selector']; ?>"/>
			</p>						
			<p>
				<label><?php _e('Sidebar Area Selector','fs'); ?></label>
				<input type="input" name="fs_sidebar_area_selector" value="<?php echo $this->fs_settings['sidebar_area_selector']; ?>"/>
			</p>	
			<br/>

			<h3><?php _e('Campaign Stats Display Settings','fs'); ?></h3>
			<p>
				<label><?php _e('Title Color','fs'); ?></label>
				<input class="fs-color-picker" type="input" name="fs_stats_title_color" value="<?php echo $this->fs_settings['stats_title_color']; ?>"/>
			</p>					
			<p>
				<label><?php _e('Background Color','fs'); ?></label>
				<input class="fs-color-picker" type="input" name="fs_stats_background_color" value="<?php echo $this->fs_settings['stats_background_color']; ?>"/>
			</p>					
			<p>
				<label><?php _e('Progress Bar Color','fs'); ?></label>
				<input class="fs-color-picker" type="input" name="fs_progress_bar_color" value="<?php echo $this->fs_settings['progress_bar_color']; ?>"/>
			</p>					
			<br/>

			<h3><?php _e('Contributors List Display Settings','fs'); ?></h3>
			<p>
				<label><?php _e('Title Color','fs'); ?></label>
				<input class="fs-color-picker" type="input" name="fs_contributors_title_color" value="<?php echo $this->fs_settings['contributors_title_color']; ?>"/>
			</p>					
			<p>
				<label><?php _e('Background Color','fs'); ?></label>
				<input class="fs-color-picker" type="input" name="fs_contributors_background_color" value="<?php echo $this->fs_settings['contributors_background_color']; ?>"/>
			</p>
			<br/>		

			<!--<h3><?php _e('Style Settings','fs'); ?></h3>
			<p class="fs-custom-css">
				<label><?php _e('Custom CSS','fs'); ?></label>
				<textarea name="fs_custom_css"><?php echo get_option('fs_custom_css'); ?></textarea>
			</p>					
			<br/>-->

			<p>
				<input class="button button-primary" type="submit" name="fs_update_settings" value="Save Settings" />
			</p>
		</form>					
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$('.fs-color-picker').wpColorPicker();
			});

		</script>	
	</div>
</div>