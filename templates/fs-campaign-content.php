<?php 
    global $post;
    $content_area = $this->fs_settings['content_area_selector']; 
?>
<div class="hideme" style="position:absolute;top:0;left:0;width:100%;height:100%;background:#fff;z-index:9999;"></div>
<div class="fs-campaign-content user-content" style="display:none;">
    <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>
            <?php if($this->fs_settings['show_campaign_title'] != 'no') : ?>
                <h1 class="fs-campaign-title"><?php the_title(); ?></h1>
            <?php endif; ?>
            <?php the_content(); ?>
            <?php if($this->fs_is_campaign_upcoming($post->ID)) : ?>
                <h3 class="fs-notice"><?php printf(__('Contributions for this campaign will begin on %1$s. Come back and show your support!','fs'),date('F jS, Y', strtotime(get_post_meta($post->ID, 'fs_campaign_start', true)))); ?></h3>
            <?php else : ?>
                <?php if($this->fs_has_campaign_ended($post->ID)) : ?>
                     <h3 class="fs-error"><?php printf(__('Contributions for this campaign ended on %1$s. Thank You for your support!','fs'),date('F jS, Y', strtotime(get_post_meta($post->ID, 'fs_campaign_end', true)))); ?></h3>
                <?php else : ?> 
                    <?php $gf_id = get_post_meta($post->ID,'fs_gravity_form',true); ?>
                    <?php echo do_shortcode('[gravityform id="'.$gf_id.'" title="false" description="false" ajax="true"]'); ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('<?php echo $content_area; ?>').html($('.fs-campaign-content'));
        $('.fs-campaign-content').removeAttr("style");
    });
</script>