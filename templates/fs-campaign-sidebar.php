<?php 
	global $post;
	$sidebar_area = $this->fs_settings['sidebar_area_selector']; 
?>
<div class="fs-campaign-sidebar user-content" style="display:none;">

	<div class="donation-stats" style="background:<?php echo get_option('fs_stats_background_color'); ?>;">
		<!--<h3 style="color:<?php echo get_option('fs_stats_title_color'); ?>;"><?php the_title(); ?></h3>-->
		<h4 style="color:<?php echo get_option('fs_stats_title_color'); ?>;"><?php _e('Campaign Details','fs'); ?></h4>
		<hr/>
		<div class="stats">
			<div class="progress-bar">
				<?php 
					// get all of this campagins contribution details				
					$raised = $this->fs_get_amount_contributed($post->ID);
					$goal = get_post_meta($post->ID, 'fs_campaign_goal', true);
					$donors = $this->fs_get_number_of_contributions($post->ID);
					$contributions = get_post_meta($post->ID,'fs_contributions',true);

					// get all micro campaign contribution details
					$micro_raised = 0;
					$micro_donors = 0;
					$micros = $this->fs_get_micro_campaigns($post->ID);
					if($micros){
						foreach($micros as $micro){
							$micro_raised += $this->fs_get_amount_contributed($micro->ID);
							$micro_donors += $this->fs_get_number_of_contributions($micro->ID);
							$micro_contributions = get_post_meta($micro->ID,'fs_contributions',true);
							if($micro_contributions && $contributions){
								$contributions = array_merge($contributions, $micro_contributions);
							}
						}
					}

					// figure out how large the progress bar is
					$total_raised = $raised + $micro_raised;
					$percent = 0;
					if($total_raised > 0){
						$percent = intval(str_replace(',','',$raised)) / intval(str_replace(',','',$goal)) * 100;
					}
					if($percent > 100){$percent = 100;}

					// format numbers
					$total_raised = number_format($total_raised);
					$goal = number_format($goal);

					// add the donors up
					$total_donors = $donors + $micro_donors;
				?>
				<div class="progress" style="width:<?php echo $percent; ?>%;background:<?php echo $this->fs_settings['progress_bar_color']; ?>;"></div>
				<div class="progress-amount">$<?php echo $total_raised; ?></div>
				<div class="progress-goal">$<?php echo $goal; ?></div>
			</div>
			<ul class="stat-list">
				<li class="left"><div><span>$<?php echo $goal; ?></span><br/><?php _e('GOAL','fs'); ?></div></li>
				<li><div><span>$<?php echo $total_raised; ?></span><br/><?php _e('RAISED','fs'); ?></div></li>
				<li class="left"><div><span><?php echo $total_donors; ?></span><br/><?php _e('DONORS','fs'); ?></div></li>
	            <?php if($this->fs_is_campaign_upcoming($post->ID)): ?>
	            	<li><div><span><?php echo $this->fs_get_campaign_days_until($post->ID); ?></span><br/><?php _e('DAYS UNTIL','fs'); ?></div></li>
	            <?php else : ?>
					<li><div><span><?php echo $this->fs_get_campaign_days_left($post->ID); ?></span><br/><?php _e('DAYS LEFT','fs'); ?></div></li>
	            <?php endif; ?>
			</ul>
			<span class="fs-clearer"></span>
		</div>
	</div>
	
	<?php if(get_post_meta($post->ID, 'fs_honor_roll_listing', true) != 'none') : ?>
		<?php if($contributions) : // test if any contributions made ?>
			<div class="top-contributors" style="background:<?php echo get_option('fs_contributors_background_color'); ?>;">

				<?php if(get_post_meta($post->ID,'fs_honor_roll_listing',true) == 'recent') : ?>
					<h4 style="color:<?php echo get_option('fs_contributors_title_color'); ?>;"><?php _e('Recent Contributors','fs'); ?></h4>
					<hr/>
					<?php
						// sort contributions by amount DESC
						rsort($contributions);
					?>
				<?php elseif(get_post_meta($post->ID,'fs_honor_roll_listing',true) == 'current') : ?>
					<h4 style="color:<?php echo get_option('fs_contributors_title_color'); ?>;"><?php _e('Contributors','fs'); ?></h4>
					<hr/>
					<?php
						// sort contributions by random
						shuffle($contributions);
					?>
				<?php elseif(get_post_meta($post->ID,'fs_honor_roll_listing',true) == 'top') : ?>
					<h4 style="color:<?php echo get_option('fs_contributors_title_color'); ?>;"><?php _e('Top Contributors','fs'); ?></h4>
					<hr/>
					<?php
						// sort contributions by amount DESC
						usort($contributions, function($a, $b) {
							return $b['amount'] - $a['amount'];
						});
					?>			
				<?php elseif(get_post_meta($post->ID,'fs_honor_roll_listing',true) == 'alphabetical') : ?>
					<h4 style="color:<?php echo get_option('fs_contributors_title_color'); ?>;"><?php _e('Contributors','fs'); ?></h4>
					<hr/>
					<?php
						// sort contributions by last name ASC
						usort($contributions, function($a, $b) {
							return strnatcmp($a['last_name'], $b['last_name']);
						});
					?>
				<?php endif; ?>
				
				<ul>
				<?php $count = 0; ?>

				<?php foreach($contributions as $contribution) : ?>
					<?php if($contribution['amount'] != 0) : ?>
						<li>
							<?php $values_display = get_post_meta($post->ID, 'fs_honor_roll_values_display', true); ?>
							<span class="fs-contribution-amount" style="color:<?php echo get_option('fs_contributors_title_color'); ?>;">
								<?php if($values_display != 'names_only') : ?>
									$<?php echo number_format($contribution['amount'],2,'.',','); ?>
								<?php endif; ?>
							</span>

							<?php $names_display = get_post_meta($post->ID, 'fs_honor_roll_names_display', true); ?>
							<span class="fs-contribution-name">
								<?php if($values_display != 'values_only') : ?>
									<?php if($names_display != 'names_anonymous') : ?>
										<?php if(strtolower($contribution['anonymous']) == 'yes') : ?>
											<?php _e('Anonymous','fs'); ?>
										<?php else : ?>
											<?php echo $contribution['first_name'] . ' ' . $contribution['last_name']; ?>
										<?php endif; ?>
									<?php else : ?>
										<?php _e('Anonymous','fs'); ?>
									<?php endif; ?>
								<?php endif; ?>
							</span>
							<span class="fs-clearer"></span>
						</li>
						<?php $count++; ?>
					<?php endif; ?>
					<?php if($count >= get_post_meta($post->ID,'fs_number_of_contributors',true)){break;} ?>
				<?php endforeach; ?>

				</ul>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php $micro_campaigns_enabled = get_post_meta($post->ID, 'fs_enable_micro_campaigns', true); ?>
	<?php if($micro_campaigns_enabled == 'yes' && !empty($this->fs_settings['create_micro_campaign_page'])) : ?>
		<a class="fs-create-micro-campaign-cta" href="<?php echo get_permalink($this->fs_settings['create_micro_campaign_page']) . '/?campaign=' . $post->ID; ?>" style="color:<?php echo get_option('fs_stats_title_color'); ?> !important;background:<?php echo get_option('fs_stats_background_color'); ?>;">
			<i class="icon-angle-right"></i><?php echo $this->fs_settings['micro_campaign_cta_text']; ?> 
		</a>
	<?php endif; ?>

</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
    	$('<?php echo $sidebar_area; ?>').html($('.fs-campaign-sidebar'));
    	$(".fs-campaign-sidebar").removeAttr("style");
    	$(".hideme").hide();
    });
</script>