<?php global $post; ?>
<?php wp_nonce_field('save_micro_campaign_meta', 'micro_campaign_meta_nonce'); ?>

<div class="fs-meta-fields">
    <h4><?php _e('PARENT CAMPAIGN','fs'); ?></h4>
    <p> 
        <?php $parent_id = get_post_meta($post->ID, 'fs_parent_campaign',true); ?>
        <?php $campaigns = $this->fs_get_campaigns(); ?>
        <?php if($campaigns) : ?>
            <select id="fs-parent-campaign" name="fs_parent_campaign" class="fs-parent-campaign">
                <option value=""> - Choose One - </option>
                <?php foreach($campaigns as $campaign): ?>
                    <option value="<?php echo $campaign->ID; ?>"<?php if($campaign->ID == $parent_id){echo ' selected="selected"';} ?>><?php echo $campaign->post_title; ?></option>
                <?php endforeach; ?>
            </select>
        <?php else : ?>
            <?php printf(__('No Campaigns have been created yet. %1$sPlease create one here first%2$s','fs'),'<a href="/wp-admin/post-new.php?post_type=fs_campaign">','</a>'); ?>
        <?php endif; ?>
    </p>
    <div class="fs-parent-campaign-details"></div>

    <h4><?php _e('GENERAL INFORMATION','fs'); ?></h4>
    <div class="fs-datetimepicker-wrap">
        <label><?php _e('Campaign Start','fs'); ?></label>
        <?php $start_datetime = get_post_meta($post->ID, 'fs_campaign_start', true); ?>
        <div class="button fs-datetimepicker-trigger" data-name="fs_campaign_start">
            <i class="icon-calendar"></i>
            <span>
                <?php 
                    if($start_datetime){
                        echo date('m/d/Y @ g:i a', strtotime($start_datetime));
                    }else{
                        echo '- select a date/time -';
                    }
                ?>
            </span>
            <input type="text" class="fs-datetimepicker" name="fs_campaign_start" value="<?php if($start_datetime){echo $start_datetime;} ?>" />
        </div>
    </div>

    <div class="fs-datetimepicker-wrap">
        <label><?php _e('Campaign End','fs'); ?></label>
        <?php $end_datetime = get_post_meta($post->ID, 'fs_campaign_end', true); ?>
        <div class="button fs-datetimepicker-trigger" data-name="fs_campaign_end">
            <i class="icon-calendar"></i>
            <span>
                <?php 
                    if($end_datetime){
                        echo date('m/d/Y @ g:i a', strtotime($end_datetime));
                    }else{
                        echo '- select a date/time -';
                    }
                ?>
            </span>
            <input type="text" class="fs-datetimepicker" name="fs_campaign_end" value="<?php if($end_datetime){echo $end_datetime;} ?>" />
        </div>
    </div>

    <p>
        <label for="fs-campaign-goal"><?php _e('Campaign Goal','fs'); ?></label>
        <input type="number" name="fs_campaign_goal" id="fs-campaign-goal" value="<?php echo get_post_meta($post->ID, 'fs_campaign_goal', true); ?>" />
    </p>
    <p>
        <label for="fs-other-contributions"><?php _e('Other Contributions','fs'); ?></label>
        <input type="number" name="fs_other_contributions" id="fs-other-contributions" value="<?php echo get_post_meta($post->ID, 'fs_other_contributions', true); ?>" />
    </p>
    
    <h4><?php _e('CONTRIBUTOR DISPLAY SETTINGS','fs'); ?></h4>
    <p>
        <label><?php _e('Honor Roll Listing Type','fs'); ?></label>
        <?php $options = array('top' => 'Top Contributors', 'recent' => 'Recent Contributors', 'current' => 'Current contributors (random)', 'none' => 'Do not list any contributors', 'alphabetical' => 'Alphabetical (ASC)'); ?>
        <select name="fs_honor_roll_listing">
            <?php $honor_roll_listing = get_post_meta($post->ID,'fs_honor_roll_listing',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($honor_roll_listing == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>    
    <p>
        <label><?php _e('Listing Names Display','fs'); ?></label>
        <?php $options = array('names_entered' => 'Names as entered', 'names_anonymous' => 'Names as anonymous'); ?>
        <select name="fs_honor_roll_names_display">
            <?php $names_display = get_post_meta($post->ID,'fs_honor_roll_names_display',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($names_display == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>    
    <p>
        <label><?php _e('Listing Values Display','fs'); ?></label>
        <?php $options = array('names_values' => 'Names and values', 'names_only' => 'Names Only', 'values_only' => 'Values only'); ?>
        <select name="fs_honor_roll_values_display">
            <?php $values_display = get_post_meta($post->ID,'fs_honor_roll_values_display',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($values_display == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <?php $num_contributors = get_post_meta($post->ID, 'fs_number_of_contributors', true); ?>
        <label for="fs-number-of-contributors"><?php _e('Maximum number of contributors to display','fs'); ?></label>
        <input type="number" name="fs_number_of_contributors" id="fs-number-of-contributors" value="<?php if($num_contributors){echo $num_contributors;}else{echo '10';} ?>" />
    </p>      

    <h4>CONTACT INFORMATION</h4>
    <p>
        <label>Name:</label>
        <input type="text" name="fs_campaign_creator_name" value="<?php echo get_post_meta($post->ID, 'fs_campaign_creator_name', true); ?>" />
    </p>    
    <p>
        <label>Email:</label>
        <input type="text" name="fs_campaign_creator_email" value="<?php echo get_post_meta($post->ID, 'fs_campaign_creator_email', true); ?>" />
    </p>

     
    <script type="text/javascript">
        jQuery(document).ready(function($){
            // datetimepickers
            $('.fs-datetimepicker').datetimepicker({
                format:'Y-m-d H:i',
                formatTime: "g:i a",
                step: 30,
                defaultDate:new Date(),
                allowBlank: true,
                onChangeDateTime:function(current_time, $input){
                    var df = new DateFormatter();
                    $input.siblings('span').text(df.formatDate(current_time, 'm/d/Y @ g:i a'));
                }
            });

            // datetimepicker triggers
            $('.fs-datetimepicker-trigger').on('click',function(){
                $('input', this).datetimepicker('show');
            });

            // ajax call to populate form fields for selected gravity form
            $('.fs-parent-campaign').on('change',function(){
                var parent_id = $(this).val();
                var data = {
                    'action': 'fs_get_parent_campaign_details',
                    'parent_id': parent_id,
                    'post_id': <?php echo $post->ID; ?>
                };
                $.post(ajaxurl, data, function(response){
                    $('.fs-parent-campaign-details').html(response); 
                }); 
            }).change();
        });
    </script>    
</div>