<?php global $post; ?>
<?php wp_nonce_field('save_campaign_meta', 'campaign_meta_nonce'); ?>

<div class="fs-meta-fields">
    <h4><?php _e('GENERAL INFORMATION','fs'); ?></h4>
    <div class="fs-datetimepicker-wrap">
        <label><?php _e('Campaign Start','fs'); ?></label>
        <?php $start_datetime = get_post_meta($post->ID, 'fs_campaign_start', true); ?>
        <input type="text" class="fs-datetimepicker" name="fs_campaign_start" value="<?php if($start_datetime){echo $start_datetime;} ?>" />
        <i class="icon-calendar"></i>
    </div>  

    <div class="fs-datetimepicker-wrap">
        <label><?php _e('Campaign End','fs'); ?></label>
        <?php $end_datetime = get_post_meta($post->ID, 'fs_campaign_end', true); ?>
        <input type="text" class="fs-datetimepicker" name="fs_campaign_end" value="<?php if($end_datetime){echo $end_datetime;} ?>" />
        <i class="icon-calendar"></i>
    </div>  

    <p>
        <label for="fs-campaign-goal"><?php _e('Campaign Goal','fs'); ?></label>
        <input type="number" name="fs_campaign_goal" id="fs-campaign-goal" value="<?php echo get_post_meta($post->ID, 'fs_campaign_goal', true); ?>" />
    </p>      
    <p>
        <label for="fs-other-contributions"><?php _e('Other Contributions','fs'); ?></label>
        <input type="number" name="fs_other_contributions" id="fs-other-contributions" value="<?php echo get_post_meta($post->ID, 'fs_other_contributions', true); ?>" />
    </p>   
    
    <h4><?php _e('GRAVITY FORM MAPPING','fs'); ?></h4>
    <p class="fs-message"><strong>IMPORTANT!</strong> Funds cannot be collected until the form selected has a feed setup with your payment gateway.</p>
    <p>
        <?php // check for gravity forms first ?>
        <?php if(class_exists('GFForms')) : ?>  
            <?php $forms = RGFormsModel::get_forms(null, 'title'); ?>
            <?php if($forms) : ?>
    			<?php $gf_id = get_post_meta($post->ID, 'fs_gravity_form',true); ?>
                <label for="fs-gravity-form"><?php _e('This Campaign\'s Gravity form','fs'); ?></label> 
                <select id="fs-gravity-form" name="fs_gravity_form" class="fs-gravity-forms">
                    <option value=""> - Choose One - </option>
                    <?php foreach( $forms as $form ): ?>
                        <option value="<?php echo $form->id; ?>"
                            <?php 
                                if(empty($gf_id)){
                                    if($form->id == $this->fs_settings['default_campaign_form_id']){echo ' selected="selected"';}
                                }else{
                                    if($form->id == $gf_id){echo ' selected="selected"';} 
                                }   
                            ?>
                        ><?php echo $form->title; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else : ?>
                No Gravity forms have been created yet. <a href="/wp-admin/admin.php?page=fs-settings">Please import the Funding Serious forms first.</a>
            <?php endif; ?>
        <?php endif; ?>
    </p>
    <div class="fs-gf-form-fields"></div>

    <h4><?php _e('CONTRIBUTOR DISPLAY SETTINGS','fs'); ?></h4>
    <p>
        <label><?php _e('Honor Roll Listing Type','fs'); ?></label>
        <?php $options = array('top' => 'Top Contributors', 'recent' => 'Recent Contributors', 'current' => 'Current contributors (random)', 'none' => 'Do not list any contributors', 'alphabetical' => 'Alphabetical (ASC)'); ?>
        <select name="fs_honor_roll_listing">
            <?php $honor_roll_listing = get_post_meta($post->ID,'fs_honor_roll_listing',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($honor_roll_listing == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>    
    <p>
        <label><?php _e('Listing Names Display','fs'); ?></label>
        <?php $options = array('names_entered' => 'Names as entered', 'names_anonymous' => 'Names as anonymous'); ?>
        <select name="fs_honor_roll_names_display">
            <?php $names_display = get_post_meta($post->ID,'fs_honor_roll_names_display',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($names_display == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>    
    <p>
        <label><?php _e('Listing Values Display','fs'); ?></label>
        <?php $options = array('names_values' => 'Names and values', 'names_only' => 'Names Only', 'values_only' => 'Values only'); ?>
        <select name="fs_honor_roll_values_display">
            <?php $values_display = get_post_meta($post->ID,'fs_honor_roll_values_display',true); ?>
            <?php foreach($options as $k => $v) : ?>
                <option value="<?php echo $k; ?>" <?php if($values_display == $k){echo 'selected="selected"';} ?>><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <?php $num_contributors = get_post_meta($post->ID, 'fs_number_of_contributors', true); ?>
        <label for="fs-number-of-contributors"><?php _e('Maximum number of contributors to display','fs'); ?></label>
        <input type="number" name="fs_number_of_contributors" id="fs-number-of-contributors" value="<?php if($num_contributors){echo $num_contributors;}else{echo '10';} ?>" />
    </p>  

    <h4><?php _e('MICRO CAMPAIGNS','fs'); ?></h4>
    <p>
        Enable Micro Campaign creation for this Campaign?<br/>
        <?php $enabled = get_post_meta($post->ID, 'fs_enable_micro_campaigns', true); ?>
        <label><input type="radio" name="fs_enable_micro_campaigns" value="no" <?php if($enabled == 'no' || empty($enabled)){echo 'checked="checked"';} ?> /> No</label><br/>
        <label><input type="radio" name="fs_enable_micro_campaigns" value="yes" <?php if($enabled == 'yes'){echo 'checked="checked"';} ?> /> Yes</label>
    </p>  
    <?php $micros = $this->fs_get_micro_campaigns($post->ID); ?>
    <?php if($micros) : ?>
        <ul>
        <li>Created Micro Campaigns:</li>
        <?php foreach($micros as $micro) : ?>
            <li><a href="/wp-admin/post.php?post=<?php echo $micro->ID; ?>&action=edit"><?php echo $micro->post_title; ?> <i class="icon-edit"></i></a></li>
        <?php endforeach; ?>
        </ul>
    <?php else : ?>
        <p>Sorry, There are no Micro Campaigns supporting this Campaign</p>
    <?php endif; ?>
     
    <script type="text/javascript">
        jQuery(document).ready(function($){
            // datetimepickers
            $('.fs-datetimepicker').datetimepicker({
                format:'Y-m-d H:i',
                formatTime: "g:i a",
                step: 30,
                defaultDate:new Date(),
                allowBlank: true,
                onChangeDateTime:function(current_time, $input){        
                    var df = new DateFormatter();
                    $input.siblings('span').text(df.formatDate(current_time, 'm/d/Y @ g:i a'));
                }
            });

            // datetimepicker triggers
            $('.fs-datetimepicker-trigger').on('click',function(){
                $('input', this).datetimepicker('show');
            });

            // ajax call to populate form fields for selected gravity form
            $('.fs-gravity-forms').on('change',function(){
                var form_id = $(this).val();
                var data = {
                    'action': 'fs_get_gravity_form_fields',
                    'form_id': form_id,
                    'post_id': <?php echo $post->ID; ?>
                };
                $.post(ajaxurl, data, function(response){
                    $('.fs-gf-form-fields').html(response); 
                }); 
            }).change();
        });
    </script>    
</div>