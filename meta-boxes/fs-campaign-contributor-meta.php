<?php global $post; ?>
<?php wp_nonce_field('save_campaign_contributor_meta', 'campaign_contributor_meta_nonce'); ?>

<div id="fs-contributor-fields">
	<div id="fs-saving-wrap" style="display:none;">
		<div id="fs-saving">
			<?php _e('REMOVING','fs'); ?><br/>
			<img src="<?php echo $this->fs_settings['dir']; ?>images/loading.gif" alt="saving"/>
		</div>
	</div>
	
	<?php $contributions = get_post_meta($post->ID,'fs_contributions',true); ?>
	<?php if($contributions) : ?>
		<?php $num_contributions = count($contributions); ?>
		<?php krsort($contributions); ?>
		<ul class="fs-contributor-list">
		<?php $count = 0; ?>
		<?php foreach($contributions as $entry_id => $contribution) :  ?>
			<li class="<?php if($count%2){echo 'fs-even';}else{echo 'fs-odd';} $count++; ?>">
				<span class="fs-contributor-info">
					<a target="_blank" href="<?php echo $contribution['entry']; ?>"><?php echo date(get_option('date_format') . ' '. get_option('time_format'), strtotime($contribution['date'])); ?></a><br/>
					<?php echo $contribution['first_name'] . ' ' . $contribution['last_name'] . ' - $'. $contribution['amount']; ?>
				</span>
				<span class="fs-contributor-remove"><i class="icon-remove" data-id="<?php echo $entry_id; ?>"></i></span>
				<span class="fs-clearer"></span>
			</li>
		<?php endforeach; ?>
		</ul>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$('.fs-contributor-remove i').on('click',function(){
					if(confirm('Are you sure you want to remove this contribution?')) {
						$('#fs-saving-wrap').fadeIn('fast');
						var meta_id = $(this).attr('data-id');
		                var data = {
		                    'action': 'fs_remove_contribution',
		                    'meta_id': meta_id,
		                    'post_id': '<?php echo $post->ID; ?>'
		                };
		                $.post(ajaxurl, data, function(response){
		                	location.reload();
		                }); 
					}
				});
			});
		</script>
	<?php else : ?>
		<p><?php _e('There haven\'t been any contributions made yet','fs'); ?></p>
	<?php endif; ?>
	<a class="button" target="_blank" href="/wp-admin/admin.php?page=fs-contribution&campaign=<?php echo $post->ID; ?>"><?php _e('Add a Contribution'); ?></a>
</div>