------------------------------------------
------- FUNDING SERIOUS CHANGELOG --------
------------------------------------------

== 1.4.5 ==
* Fixed display error when no other contributions have been made

== 1.4.4 ==
* Removed the "button" styling for campaign meta
* Changed the micro campaign form to submit via ajax

== 1.4.3 ==
* Fixing array merge in sidebar

== 1.4.2 ==
* Added notification for micro campaigns when published

== 1.4.1 ==
* Fixed "add a contribution" button link

== 1.4 ==
* Added new display options for the "Honor roll"
* Added "show campaign title" option

== 1.3 ==
* Added Gravity Forms requirement for plugin to be active
* Fixed warning on settings -> add contribution tab when no $_GET isset
* Added default campaign form to auto generate if not exists
* Added default campaign form selector to plugin settings
* Added auto detect form/fields from default on new campaigns
* Added "micro campaigns"