<?php
    /*
    Plugin Name:            Funding Serious
    Plugin URI:             http://buildcreate.com
    Description:            A platform to allow for users to collect funds towards created campaign goals
    Version:                1.4.5
    Author:                 buildcreate
    Author URI:             http://buildcreate.com
    Text Domain:            nes
    Contributors:           kraftpress, buildcreate, a2rocklobster
    License:                GNU General Public License v2
    License URI:            http://www.gnu.org/licenses/gpl-2.0.html
    Bitbucket Plugin URI:   https://bitbucket.org/buildcreatestudios/funding-serious
    */

    // If this file is called directly, abort.
    if(!defined('WPINC')){die;}

    // main class
    class FundingSerious {
        public $fs_settings;
        public $fs_function_vars;
        
        function __construct(){

            // set wordpress timezone
            date_default_timezone_set(get_option('timezone_string'));

            // helpers
            add_filter('fs_get_path', array($this, 'fs_get_path'), 1, 1);
            add_filter('fs_get_dir', array($this, 'fs_get_dir'), 1, 1);

             // vars
            $this->fs_settings = array(
                'version' => '1.4.5',
                'path' => apply_filters('fs_get_path', __FILE__),
                'dir' => apply_filters('fs_get_dir', __FILE__),
                'hook' => basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ),
                'stats_title_color' => '#f0dc51',
                'stats_background_color' => '#3580c0',
                'progress_bar_color' => '#f0dc51',
                'contributors_title_color' => '#242d6e',
                'contributors_background_color' => '#f0dc51',
                'content_area_selector' => '.page-content',
                'sidebar_area_selector' => '.sidebar',
                'default_campaign_form_id' => '',
                'micro_campaign_form_id' => '',
                'create_micro_campaign_page' => '',
                'micro_campaign_cta_text' => 'Help Raise Funds for this Campaign by creating your own Micro Campaign!',
                'show_campaign_title' => 'yes'
            );

            // function variables
            $this->fs_function_vars = array(
                'active_feeds' => array()
            );

            // actions
            add_action('admin_menu', array($this, 'admin_menu'), 999);
            add_action('admin_head', array($this, 'admin_menu_highlight'));
            add_action('init', array($this, 'init'), 1);
            add_action('admin_enqueue_scripts', array($this, 'fs_styles'));
            add_action('wp_enqueue_scripts', array($this, 'fs_styles'));

            // add body class if necessary
            add_filter('body_class', array($this, 'fs_body_classes'));

            // campaigns
            add_action('add_meta_boxes', array($this, 'fs_campaign_meta_box'));
            add_action('save_post', array($this, 'fs_save_campaign_meta'),1);     
            add_action('add_meta_boxes', array($this, 'fs_campaign_contributor_meta_box'));       
            add_action('wp_head', array($this,'fs_add_campaign_content_template'));
            add_action('wp_head', array($this,'fs_add_campaign_sidebar_template'));

            // micro campaigns
            add_action('add_meta_boxes', array($this, 'fs_micro_campaign_meta_box'));
            add_action('save_post', array($this, 'fs_save_micro_campaign_meta'),1);            
            add_action('add_meta_boxes', array($this, 'fs_micro_campaign_contributor_meta_box'));            
            add_action('wp_head', array($this,'fs_add_micro_campaign_content_template'));
            add_action('wp_head', array($this,'fs_add_micro_campaign_sidebar_template'));
            add_action( 'transition_post_status', array($this, 'fs_post_status_transition'), 10, 3); 

            // ajax
            add_action('wp_ajax_fs_get_gravity_form_fields', array($this, 'fs_get_gravity_form_fields'));
            add_action('wp_ajax_fs_get_parent_campaign_details', array($this, 'fs_get_parent_campaign_details'));
            add_action('wp_ajax_fs_remove_contribution', array($this, 'fs_remove_contribution'));

            // check for Gravity forms
            add_action('wp_loaded', array($this,'fs_check_for_gf'));
            
            // localization
            // add_action('plugins_loaded', array($this, 'fs_load_textdomain'));

            // shortcodes
            add_shortcode('fs-campaign-list', array($this, 'fs_campaign_list'));
            add_shortcode('fs-micro-campaign-form', array($this, 'fs_micro_campaign_form'));

            // add widget
            add_action('widgets_init', function(){register_widget('FsCampaignList');});
        }

        function init(){
			
            // set options
            if(isset($_POST['fs_update_settings'])){
                update_option('fs_show_campaign_title', sanitize_text_field($_POST['fs_show_campaign_title']));
                update_option('fs_stats_title_color', sanitize_text_field($_POST['fs_stats_title_color']));
                update_option('fs_stats_background_color', sanitize_text_field($_POST['fs_stats_background_color']));
                update_option('fs_progress_bar_color', sanitize_text_field($_POST['fs_progress_bar_color']));
                update_option('fs_contributors_title_color', sanitize_text_field($_POST['fs_contributors_title_color']));
                update_option('fs_contributors_background_color', sanitize_text_field($_POST['fs_contributors_background_color']));
                update_option('fs_content_area_selector', sanitize_text_field($_POST['fs_content_area_selector']));
                update_option('fs_sidebar_area_selector', sanitize_text_field($_POST['fs_sidebar_area_selector']));
                update_option('fs_default_campaign_form_id', sanitize_text_field($_POST['fs_default_campaign_form_id']));
                update_option('fs_create_micro_campaign_page', sanitize_text_field($_POST['fs_create_micro_campaign_page']));
                update_option('fs_micro_campaign_cta_text', sanitize_text_field($_POST['fs_micro_campaign_cta_text']));
            }
            
            // get options
            $show_campaign_title = get_option('fs_show_campaign_title');
            if(!empty($show_campaign_title)){$this->fs_settings['show_campaign_title'] = $show_campaign_title;}  

            $stats_title_color = get_option('fs_stats_title_color');
            if(!empty($stats_title_color)){$this->fs_settings['stats_title_color'] = $stats_title_color;}               

            $stats_background_color = get_option('fs_stats_background_color');
            if(!empty($stats_background_color)){$this->fs_settings['stats_background_color'] = $stats_background_color;}            

            $progress_bar_color = get_option('fs_progress_bar_color');
            if(!empty($progress_bar_color)){$this->fs_settings['progress_bar_color'] = $progress_bar_color;}            

            $contributors_title_color = get_option('fs_contributors_title_color');
            if(!empty($contributors_title_color)){$this->fs_settings['contributors_title_color'] = $contributors_title_color;}            

            $contributors_background_color = get_option('fs_contributors_background_color');
            if(!empty($contributors_background_color)){$this->fs_settings['contributors_background_color'] = $contributors_background_color;}

            $content_area_selector = get_option('fs_content_area_selector');
            if(!empty($content_area_selector)){$this->fs_settings['content_area_selector'] = $content_area_selector;}            

            $sidebar_area_selector = get_option('fs_sidebar_area_selector');
            if(!empty($sidebar_area_selector)){$this->fs_settings['sidebar_area_selector'] = $sidebar_area_selector;}            

            $campaign_form_id = get_option('fs_default_campaign_form_id');
            if(!empty($campaign_form_id)){$this->fs_settings['default_campaign_form_id'] = $campaign_form_id;}              

            $micro_form_id = get_option('fs_micro_campaign_form_id');
            if(!empty($micro_form_id)){$this->fs_settings['micro_campaign_form_id'] = $micro_form_id;}            

            $create_micro_campaign_page = get_option('fs_create_micro_campaign_page');
            if(!empty($create_micro_campaign_page)){$this->fs_settings['create_micro_campaign_page'] = $create_micro_campaign_page;}
            
            $micro_campaign_cta_text = get_option('fs_micro_campaign_cta_text');
            if(!empty($micro_campaign_cta_text)){$this->fs_settings['micro_campaign_cta_text'] = $micro_campaign_cta_text;}


            // Create campaign post type
            $labels = array(
                'campaign' => array(
                    'name' => 'Campaigns',
                    'singular_name' => 'Campaign',
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New Campaign',
                    'edit_item' =>  'Edit Campaign',
                    'new_item' => 'New Campaign',
                    'view_item' => 'View Campaign',
                    'search_items' => 'Search Campaigns',
                    'not_found' => 'No Campaigns Found',
                    'not_found_in_trash' => 'No Campaigns found in Trash'
                ),
                'micro_campaign' => array(
                    'name' => 'Micro Campaigns',
                    'singular_name' => 'Micro Campaign',
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New Micro Campaign',
                    'edit_item' =>  'Edit Micro Campaign',
                    'new_item' => 'New Micro Campaign',
                    'view_item' => 'View Micro Campaign',
                    'search_items' => 'Search Micro Campaigns',
                    'not_found' => 'No Micro Campaigns Found',
                    'not_found_in_trash' => 'No Micro Campaigns found in Trash'
                )
            );
            
            foreach($labels as $k => $label){
                register_post_type('fs_'.$k, array(
                    'labels' => $label,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'rewrite' => array('slug' => str_replace('_', '-', $k)),
                    'query_var' => 'fs_'.$k,
                    'supports' => array('title','editor','thumbnail','revisions'),
                    'show_in_menu'  => false,
                    'show_in_admin_bar'  => true
                ));     
            }
        }


        /////////////////////////////
        ////////// menus ////////////
        /////////////////////////////

        function admin_menu(){
            global $submenu;
            add_menu_page(__('Funding Serious','fs'), __('Funding Serious','fs'), 'manage_options', 'fs-settings', null, null, '6.66');
            add_submenu_page('fs-settings', __('Campaigns','fs'), __('Campaigns','fs'), 'manage_options', 'edit.php?post_type=fs_campaign');
            add_submenu_page('fs-settings', __('Micro Campaigns','fs'), __('Micro Campaigns','fs'), 'manage_options', 'edit.php?post_type=fs_micro_campaign');
            add_submenu_page('fs-settings', __('Add Contribution','fs'), __('Add Contribution','fs'), 'manage_options', 'fs-contribution', array($this, 'fs_contribution_callback'));
            add_submenu_page('fs-settings', __('Settings','fs'), __('Settings','fs'), 'manage_options', 'fs-settings', array($this, 'fs_settings_callback'));
            unset($submenu['fs-settings'][0]);
        }

        function admin_menu_highlight() {
            global $parent_file, $submenu_file, $post_type;
            switch($post_type){
                case 'fs_campaign' :
                case 'fs_micro_campaign' :
                    $parent_file = 'fs-settings'; break;
                default : break;
            }
        }

        function fs_contribution_callback(){
            include_once 'fs-add-contribution.php';
        }          

        function fs_settings_callback(){
            include_once 'fs-settings.php';
        }    

        function fs_styles() { 
            wp_enqueue_style('fs-font-awesome', $this->fs_settings['dir'] . 'css/fonts.css', false, $this->fs_settings['version']); 
            wp_enqueue_script('fs-datetimepicker-js', $this->fs_settings['dir'] . 'js/datetimepicker/jquery.datetimepicker.full.min.js', array('jquery'), $this->fs_settings['version']); 
            wp_enqueue_style('fs-datetimepicker-css', $this->fs_settings['dir'] . 'js/datetimepicker/jquery.datetimepicker.min.css', false, $this->fs_settings['version']); 

            if(is_admin()){
                wp_enqueue_style('wp-color-picker');
                wp_enqueue_script( 'wp-color-picker');
                wp_enqueue_style('fs-admin-css', $this->fs_settings['dir'] . 'css/fs-admin.css', false, $this->fs_settings['version']); 
                wp_enqueue_style('fs-datepicker-css', $this->fs_settings['dir'] . 'css/fs-datepicker.css', false, $this->fs_settings['version']); 
            }else{
                wp_enqueue_script('touch-punch', $this->fs_settings['dir'] . 'js/jquery.ui.touch-punch.min.js', array('jquery'), $this->fs_settings['version']); 
                wp_enqueue_style('fs-css', $this->fs_settings['dir'] . 'css/fs.css', false, $this->fs_settings['version']); 
            }
        }

        //////////////////////////
        /////// localization /////
        //////////////////////////

        // add localization
        // function fs_load_textdomain() {
        //     load_plugin_textdomain('fs', false, basename( dirname( __FILE__ ) ) . '/lang' );
        // }


        ///////////////////////////
        ////// campaigns //////////
        ///////////////////////////

        function fs_campaign_meta_box(){
            add_meta_box('fs-campaign-meta', __('Campaign Details','fs'), array($this, 'fs_campaign_meta_box_html'), 'fs_campaign', 'normal', 'high');
        }

        function fs_campaign_meta_box_html(){
            include_once 'meta-boxes/fs-campaign-meta.php';
        }           

        // save campaign post meta 
        function fs_save_campaign_meta($post_id){
            // Check if our nonce is set.
            if(!isset($_POST['campaign_meta_nonce'])){return;}
            
            // Verify that the nonce is valid.
            if(!wp_verify_nonce($_POST['campaign_meta_nonce'], 'save_campaign_meta')){return;}
            
            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){return;}
            
            // Check the user's permissions.
            if(isset($_POST['post_type']) && 'fs_campaign' == $_POST['post_type']){
                if(!current_user_can('edit_page', $post_id)){return;}
                if(!current_user_can('edit_post', $post_id)){return;}
            }

            // sanitize data
            $start = sanitize_text_field($_POST['fs_campaign_start']);
            $end = sanitize_text_field($_POST['fs_campaign_end']);
            $goal = sanitize_text_field($_POST['fs_campaign_goal']);
            $other_contributions = sanitize_text_field($_POST['fs_other_contributions']);
            $gf = sanitize_text_field($_POST['fs_gravity_form']);
            $first_name = sanitize_text_field($_POST['fs_gf_first_name']);
            $last_name = sanitize_text_field($_POST['fs_gf_last_name']);
            $anonymous = sanitize_text_field($_POST['fs_gf_anonymous']);
            $contribution_amount = sanitize_text_field($_POST['fs_gf_contribution_amount']);
            $honor_roll_listing = sanitize_text_field($_POST['fs_honor_roll_listing']);
            $honor_roll_names_display = sanitize_text_field($_POST['fs_honor_roll_names_display']);
            $honor_roll_values_display = sanitize_text_field($_POST['fs_honor_roll_values_display']);
            $number_of_contributors = sanitize_text_field($_POST['fs_number_of_contributors']);
            $enable_micro_campaigns = sanitize_text_field($_POST['fs_enable_micro_campaigns']);

            // Update the meta field in the database.
            update_post_meta($post_id, 'fs_campaign_start', $start);
            update_post_meta($post_id, 'fs_campaign_end', $end);
            update_post_meta($post_id, 'fs_campaign_goal', $goal);
            update_post_meta($post_id, 'fs_other_contributions', $other_contributions);
            update_post_meta($post_id, 'fs_gravity_form', $gf);
            update_post_meta($post_id, 'fs_gf_first_name', $first_name);
            update_post_meta($post_id, 'fs_gf_last_name', $last_name);
            update_post_meta($post_id, 'fs_gf_anonymous', $anonymous);
            update_post_meta($post_id, 'fs_gf_contribution_amount', $contribution_amount);
            update_post_meta($post_id, 'fs_honor_roll_listing', $honor_roll_listing);
            update_post_meta($post_id, 'fs_honor_roll_names_display', $honor_roll_names_display);
            update_post_meta($post_id, 'fs_honor_roll_values_display', $honor_roll_values_display);
            update_post_meta($post_id, 'fs_number_of_contributors', $number_of_contributors);
            update_post_meta($post_id, 'fs_enable_micro_campaigns', $enable_micro_campaigns);
        }            

        // campaign contributor meta box
        function fs_campaign_contributor_meta_box(){
            add_meta_box('fs-campaign-contributor-meta', __('Campaign Contributors','fs'), array($this, 'fs_campaign_contributor_meta_box_html'), 'fs_campaign', 'side', 'default');
        }

        function fs_campaign_contributor_meta_box_html(){
            include_once 'meta-boxes/fs-campaign-contributor-meta.php';
        }    

        // add single campaign content page template
        function fs_add_campaign_content_template(){
            if('fs_campaign' == get_post_type()){
                include_once 'templates/fs-campaign-content.php';
            }
        }               

        // add single campaign sidebar page template
        function fs_add_campaign_sidebar_template(){
            if('fs_campaign' == get_post_type()){
                include_once 'templates/fs-campaign-sidebar.php';
            }
        }   


        ////////////////////////////////
        /////// micro campaigns  ///////
        ////////////////////////////////

        function fs_micro_campaign_meta_box(){
            add_meta_box('fs-micro-campaign-meta', __('Micro Campaign Details','fs'), array($this, 'fs_micro_campaign_meta_box_html'), 'fs_micro_campaign', 'normal', 'high');
        }

        function fs_micro_campaign_meta_box_html(){
            include_once 'meta-boxes/fs-micro-campaign-meta.php';
        }           

        // save micro campaign post meta 
        function fs_save_micro_campaign_meta($post_id){
            // Check if our nonce is set.
            if(!isset($_POST['micro_campaign_meta_nonce'])){return;}
            
            // Verify that the nonce is valid.
            if(!wp_verify_nonce($_POST['micro_campaign_meta_nonce'], 'save_micro_campaign_meta')){return;}
            
            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){return;}
            
            // Check the user's permissions.
            if(isset($_POST['post_type']) && 'fs_micro_campaign' == $_POST['post_type']){
                if(!current_user_can('edit_page', $post_id)){return;}
                if(!current_user_can('edit_post', $post_id)){return;}
            }

            // sanitize data 
            $parent = sanitize_text_field($_POST['fs_parent_campaign']);
            $start = sanitize_text_field($_POST['fs_campaign_start']);
            $end = sanitize_text_field($_POST['fs_campaign_end']);
            $goal = sanitize_text_field($_POST['fs_campaign_goal']);
            $other_contributions = sanitize_text_field($_POST['fs_other_contributions']);
            $honor_roll_listing = sanitize_text_field($_POST['fs_honor_roll_listing']);
            $honor_roll_names_display = sanitize_text_field($_POST['fs_honor_roll_names_display']);
            $honor_roll_values_display = sanitize_text_field($_POST['fs_honor_roll_values_display']);
            $number_of_contributors = sanitize_text_field($_POST['fs_number_of_contributors']);
            $campaign_creator_name = sanitize_text_field($_POST['fs_campaign_creator_name']);
            $campaign_creator_email = sanitize_text_field($_POST['fs_campaign_creator_email']);

            // Update the meta field in the database.
            update_post_meta($post_id, 'fs_campaign_start', $start);
            update_post_meta($post_id, 'fs_campaign_end', $end);
            update_post_meta($post_id, 'fs_campaign_goal', $goal);
            update_post_meta($post_id, 'fs_other_contributions', $other_contributions);
            update_post_meta($post_id, 'fs_parent_campaign', $parent);
            update_post_meta($post_id, 'fs_honor_roll_listing', $honor_roll_listing);
            update_post_meta($post_id, 'fs_honor_roll_names_display', $honor_roll_names_display);
            update_post_meta($post_id, 'fs_honor_roll_values_display', $honor_roll_values_display);
            update_post_meta($post_id, 'fs_number_of_contributors', $number_of_contributors);
            update_post_meta($post_id, 'fs_campaign_creator_name', $campaign_creator_name);
            update_post_meta($post_id, 'fs_campaign_creator_email', $campaign_creator_email);
        }       

        // check for change in post status
        function fs_post_status_transition($new_status, $old_status, $post){
            if(get_post_type($post->ID) == 'fs_micro_campaign'){
                if(($old_status != 'publish') && ($new_status == 'publish')){
                    $to = get_post_meta($post->ID, 'fs_campaign_creator_email', true);
                    $subject = 'Congratulations! ' . $post->post_title . ' campaign has been approved!';
                    $message = '<h1>Congratulations! ' . $post->post_title . ' campaign has been approved!</h1>';
                    $message .= '<p>Your campaign is now active and can be viewed at: <a target="_blank" href="'.get_permalink($post->ID).'">'.get_permalink($post->ID).'</a></p>';
                    add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
                    wp_mail($to, $subject, $message);
                }
            }
        }


        // micro campaign contributor meta box
        function fs_micro_campaign_contributor_meta_box(){
            add_meta_box('fs-micro-campaign-contributor-meta', __('Micro Campaign Contributors','fs'), array($this, 'fs_micro_campaign_contributor_meta_box_html'), 'fs_micro_campaign', 'side', 'default');
        }

        function fs_micro_campaign_contributor_meta_box_html(){
            include_once 'meta-boxes/fs-campaign-contributor-meta.php';
        }    

        // add single campaign content page template
        function fs_add_micro_campaign_content_template(){
            if('fs_micro_campaign' == get_post_type()){
                include_once 'templates/fs-micro-campaign-content.php';
            }
        }               

        // add single campaign sidebar page template
        function fs_add_micro_campaign_sidebar_template(){
            if('fs_micro_campaign' == get_post_type()){
                include_once 'templates/fs-micro-campaign-sidebar.php';
            }
        }   



        ///////////////////////////////////
        /////////// FUNCTIONS /////////////
        ///////////////////////////////////        

        // body class
        function fs_body_classes($classes){
            global $post;

            if($post->ID == $this->fs_settings['create_micro_campaign_page']){
                $classes[] = 'fs-create-micro-campaign';
            }
             
            return $classes;
        }


        ///////////////////////////////////
        ///////// ajax requests ///////////
        ///////////////////////////////////


        // ajax from micro campaign
        function fs_get_parent_campaign_details(){
            $parent_id = $_POST['parent_id'];
            $post_id = $_POST['post_id'];
            $start = get_post_meta($parent_id,'fs_campaign_start', true);
            $end = get_post_meta($parent_id,'fs_campaign_end', true);
            $goal = get_post_meta($parent_id,'fs_campaign_goal', true);
            ob_start();
            ?>
            <ul>
                <li><a href="/wp-admin/post.php?post=<?php echo $parent_id; ?>&action=edit"><?php echo get_the_title($parent_id); ?> <i class="icon-edit"></i></a></li>
                <li><label>Parent Start:</label><?php echo date('m/d/Y @ g:i a', strtotime($start)); ?></li>
                <li><label>Parent End:</label><?php echo date('m/d/Y @ g:i a', strtotime($end)); ?></li>
                <li><label>Parent Goal:</label><?php echo $goal; ?></li>
            </ul>
            <?php
            echo ob_get_clean();
            die();
        }

        // ajax request - campaign meta
        function fs_get_gravity_form_fields(){
            $form_id = $_POST['form_id'];
            $post_id = $_POST['post_id'];
            $default = false;
            if($form_id == $this->fs_settings['default_campaign_form_id']){$default = true;}
            ob_start();  ?>
            <p class="fs-gf-first-name">
                <label><?php _e('First Name','fs'); ?></label>
                <?php 
                    if($default){
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_first_name', '23.3');
                    }else{
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_first_name', get_post_meta($post_id, 'fs_gf_first_name', true));
                    }
                 ?>
            </p>    
            <p class="fs-gf-last-name">
                <label><?php _e('Last Name','fs'); ?></label>
                <?php 
                    if($default){
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_last_name', '23.6');
                    }else{
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_last_name', get_post_meta($post_id, 'fs_gf_last_name', true));
                    }
                ?>
            </p>            
            <p class="fs-gf-anonymous">
                <label><?php _e('Anonymous (yes/no)','fs'); ?></label>
                <?php 
                    if($default){
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_anonymous', '16');
                    }else{
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_anonymous', get_post_meta($post_id, 'fs_gf_anonymous', true));
                    }
                ?>
            </p> 
            <p class="fs-gf-contribution-amount">
                <label><?php _e('Contribution Amount','fs'); ?></label>
                <?php
                    if($default){
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_contribution_amount', '22');
                    }else{
                        echo $this->fs_get_gf_form_fields_select($form_id, 'fs_gf_contribution_amount', get_post_meta($post_id, 'fs_gf_contribution_amount', true));
                    }
                ?>
            </p>    
            <?php echo $post->ID; 
            echo ob_get_clean();
            die();
        }         

        // ajax request - remove contribution from admin
        function fs_remove_contribution(){
            $post_id = $_POST['post_id'];
            $meta_id = $_POST['meta_id'];
            $contributions = get_post_meta($post_id,'fs_contributions',true); 
            unset($contributions[$meta_id]);

            if(!empty($contributions)){
                update_post_meta($post_id,'fs_contributions',$contributions);
            }else{
                update_post_meta($post_id,'fs_contributions','');
            }
            die();
        }


        ///////////////////////////////////////////
        ///////// GRAVITY FORM ACTIONS ////////////
        ///////////////////////////////////////////

        // make sure gravity forms is active
        function fs_check_for_gf(){

             // gravity forms is required
            if(!class_exists('GFFormsModel')){

                add_action('admin_notices', array($this, 'fs_plugin_admin_notice'));            
                add_action('admin_init', array($this, 'fs_plugin_deactivate'));            
                
                if(isset($_GET['activate'])){
                     unset($_GET['activate']);
                }
            }else{    

                // import default form if triggered
                if(isset($_POST['fs_import_default_campaign_form'])){
                   $this->fs_import_campaign_gform();
                }         

                // add gravity forms campaign submission action
                add_action('gform_after_submission', array($this,'fs_campaign_form_submission'), 10, 2);   

                // add gravity forms create campaign submission action
                add_action('gform_after_submission', array($this,'fs_create_campaign_form_submission'), 10, 2);     

                //  ADMIN CONTRIBUTION GRAVITY FORMS FILTERS
                if(is_admin() && isset($_GET['page']) && $_GET['page'] == 'fs-contribution'){

                    // gravity forms "add a contribution" 
                    add_filter('gform_pre_render', array($this,'fs_remove_cc_on_admin'));
                    add_filter('gform_pre_validation', array($this,'fs_remove_cc_on_admin'));
                    add_filter('gform_pre_submission_filter', array($this,'fs_remove_cc_on_admin'));

                    // toggle feed off for backend submissions
                    add_filter('gform_validation', array($this, 'fs_maybe_disable_aim_feed_on_backend'));
                    add_action('gform_after_submission', array($this, 'fs_maybe_enable_aim_feed_on_backend'), 10, 2);

                    // change confirmation redirect to just stay on the backend
                    add_filter('gform_confirmation', array($this, 'fs_backend_confirmation'), 10, 4);

                    // add gravity forms admin submission action
                    add_action('gform_after_submission', array($this,'fs_admin_form_submission'), 10, 2);
                }
            }
        }

        function fs_plugin_admin_notice(){
            echo '<div class="error"><p><strong>Funding Serious</strong> requires Gravity Forms to function properly and has been <strong>deactivated</strong>.</p></div>';
        }

        function fs_plugin_deactivate(){
            deactivate_plugins( plugin_basename( __FILE__ ) );
        }


        // import and set as default campaign form
        function fs_import_campaign_gform(){

            // only run if gravity forms exists
            if(class_exists('GFForms')){
                $has_default_form = false;
                $has_create_form = false;

                $forms = GFFormsModel::get_forms();
                if($forms){
                    foreach($forms as &$form){
                        // check for default
                        if($form->title == 'Funding Serious - Default Campaign'){
                            $has_default_form = true;
                        }                   

                        // check for create micro campaign     
                        if($form->title == 'Funding Serious - Create Campaign'){
                            $has_create_form = true;
                        }
                    }
                }

                // create default form if neccessary
                if(!$has_default_form){
                    GFExport::import_file($this->fs_settings['path'].'inc/gf-campaign-default.json');

                    // set the default to the form created
                    $forms = GFFormsModel::get_forms();
                    if($forms){
                        foreach($forms as &$form){
                            if($form->title == 'Funding Serious - Default Campaign'){
                                update_option('fs_default_campaign_form_id', $form->id);
                                $this->fs_settings['default_campaign_form_id'] = $form->id;
                            }
                        }
                    }
                }

                // create micro campaign form is neccessary
                if(!$has_create_form){
                    GFExport::import_file($this->fs_settings['path'].'inc/gf-campaign-create.json');

                    // set the default to the form created
                    $forms = GFFormsModel::get_forms();
                    if($forms){
                        foreach($forms as &$form){
                            if($form->title == 'Funding Serious - Create Campaign'){
                                update_option('fs_micro_campaign_form_id', $form->id);
                                $this->fs_settings['create_campaign_form_id'] = $form->id;
                            }
                        }
                    }
                }
            }
        }

        function fs_get_gf_form_fields_select($form_id, $select_id, $meta_value){
            $form = RGFormsModel::get_form_meta($form_id);
            $fields = array();

            // get the form fields
            if(is_array($form["fields"])){
                foreach($form["fields"] as $field){
                    if(isset($field["inputs"]) && is_array($field["inputs"])){

                        foreach($field["inputs"] as $input)
                            $fields[] =  array($input["id"], GFCommon::get_label($field, $input["id"]));
                    }
                    else if(!rgar($field, 'displayOnly')){
                        $fields[] =  array($field["id"], GFCommon::get_label($field));
                    }
                }
            }

            // make the select box
            $select = 'No fields found for this form';
            if($fields){
                $select = '<select name="'.$select_id.'">';
                foreach($fields as $field){
                    $select .= '<option value="'.$field[0].'" '; if($field[0] == $meta_value){$select .= 'selected="selected"';} $select .= '>'.$field[1].'</option>';
                }
                $select .= '</select>';
            }

            return $select;
        }

        // remove credit card field on "add a contribution"
        function fs_remove_cc_on_admin($form){

            // only admin
            if(is_admin()){
                if($form){
                    $fields_to_hide = array('creditcard','honeypot');
                    foreach($form['fields'] as $key=>$field){
                        if(in_array($field['type'], $fields_to_hide)){
                            unset($form['fields'][$key]);
                        }                              
                    }
                    
                }
            }
            return $form;
        }

        // disable feed off for backend submissions
        function fs_maybe_disable_aim_feed_on_backend($validation_result){

            // only admin
            if(is_admin()){ 

                // only disable if valid
                if($validation_result['is_valid'] == true){

                    // get form
                    $form = $validation_result['form'];
                    if($form){
                          
                        $feeds = $this->fs_gf_get_active_feeds($form['id']);        
                        if($feeds){
                            foreach($feeds as $feed){
                                $this->fs_function_vars['active_feeds'][] = $feed['id'];
                                $result = $this->fs_gf_update_feed_active($feed['id'], false);
                            }
                        }
                    }
                }
            }

            return $validation_result;
        }       

        // enable feed off for backend submissions
        function fs_maybe_enable_aim_feed_on_backend($entry, $form){

            // only admin
            if(is_admin()){

                if($form){
                    $feeds = $this->fs_function_vars['active_feeds'];
                    if(!(empty($feeds)) && is_array($feeds)){
                        foreach($feeds as $feed_id){
                            $result = $this->fs_gf_update_feed_active($feed_id, true);
                        }
                        $this->fs_function_vars['active_feeds'] = array();
                    }
                }
            }
        }

        // if backend submission just stay on backend
        function fs_backend_confirmation($confirmation, $form, $entry, $ajax){
            if(is_admin()){
                if($form){
                    $confirmation = "Campaign submission has been logged.";
                }
            }
            return $confirmation;
        }

        // gravity forms get_active_feeds function
        function fs_gf_get_active_feeds($form_id){
            global $wpdb;

            $form_filter = is_numeric($form_id) ? $wpdb->prepare('AND form_id=%d', absint($form_id)) : '';

            $sql = $wpdb->prepare(
                "SELECT * FROM {$wpdb->prefix}gf_addon_feed
                                   WHERE addon_slug=%s AND is_active=1 {$form_filter}", 'gravityformsauthorizenet'
            );

            $results = $wpdb->get_results( $sql, ARRAY_A );
            foreach($results as &$result){
                $result['meta'] = json_decode( $result['meta'], true );
            }

            return $results;
        }

        // gravity forms update_feed_active function
        function fs_gf_update_feed_active($feed_id, $is_active){
            global $wpdb;
            $is_active = $is_active ? '1' : '0';

            $wpdb->update( "{$wpdb->prefix}gf_addon_feed", array( 'is_active' => $is_active ), array( 'id' => $feed_id ), array( '%d' ), array( '%d' ) );

            return $wpdb->rows_affected > 0;
        }

        // frontend campaign forms submission
        function fs_campaign_form_submission($entry, $form){
            global $post;
            if(get_post_type($post) == 'fs_campaign' || get_post_type($post) == 'fs_micro_campaign'){
                $campaign_id = $post->ID;
            }else{
                return;
            }

            // check for micro campaign
            if($parent_id = get_post_meta($campaign_id, 'fs_parent_campaign', true)){
                $first_name = get_post_meta($parent_id,'fs_gf_first_name',true);
                $last_name = get_post_meta($parent_id,'fs_gf_last_name',true);
                $anonymous = get_post_meta($parent_id,'fs_gf_anonymous',true);
                $amount = get_post_meta($parent_id,'fs_gf_contribution_amount',true);
                $contributions = get_post_meta($campaign_id,'fs_contributions',true);
            }else{
                $first_name = get_post_meta($campaign_id,'fs_gf_first_name',true);
                $last_name = get_post_meta($campaign_id,'fs_gf_last_name',true);
                $anonymous = get_post_meta($campaign_id,'fs_gf_anonymous',true);
                $amount = get_post_meta($campaign_id,'fs_gf_contribution_amount',true);
                $contributions = get_post_meta($campaign_id,'fs_contributions',true);
            }

            $submission = array(
                'first_name' => rgar($entry,$first_name),
                'last_name' => rgar($entry,$last_name),
                'anonymous' => rgar($entry,$anonymous),
                'amount' => rgar($entry,$amount),
                'date' => date('Y-m-d H:i'),
                'entry' => '/wp-admin/admin.php?page=gf_entries&view=entry&id='.rgar($entry,'form_id').'&lid='.rgar($entry,'id').'&dir=DESC&filter&paged=1&pos=0&field_id&operator'
            );

            if(is_array($contributions)){
                $contributions[rgar($entry,'id')] = $submission;
            }else{
                $contributions = array(rgar($entry,'id') => $submission);
            }
            update_post_meta($campaign_id,'fs_contributions',$contributions);
        }        

        // backend contribution
        function fs_admin_form_submission($entry, $form){
            global $post;
            if(is_admin()){
                $url = explode('campaign=', $entry['source_url']);
                $campaign_id = $url[1];
            }else{
                return;
            }

            // check for micro campaign
            if($parent_id = get_post_meta($campaign_id, 'fs_parent_campaign', true)){
                $first_name = get_post_meta($parent_id,'fs_gf_first_name',true);
                $last_name = get_post_meta($parent_id,'fs_gf_last_name',true);
                $anonymous = get_post_meta($parent_id,'fs_gf_anonymous',true);
                $amount = get_post_meta($parent_id,'fs_gf_contribution_amount',true);
                $contributions = get_post_meta($campaign_id,'fs_contributions',true);
            }else{
                $first_name = get_post_meta($campaign_id,'fs_gf_first_name',true);
                $last_name = get_post_meta($campaign_id,'fs_gf_last_name',true);
                $anonymous = get_post_meta($campaign_id,'fs_gf_anonymous',true);
                $amount = get_post_meta($campaign_id,'fs_gf_contribution_amount',true);
                $contributions = get_post_meta($campaign_id,'fs_contributions',true);
            }

            $submission = array(
                'first_name' => rgar($entry,$first_name),
                'last_name' => rgar($entry,$last_name),
                'anonymous' => rgar($entry,$anonymous),
                'amount' => rgar($entry,$amount),
                'date' => date('Y-m-d H:i'),
                'entry' => '/wp-admin/admin.php?page=gf_entries&view=entry&id='.rgar($entry,'form_id').'&lid='.rgar($entry,'id').'&dir=DESC&filter&paged=1&pos=0&field_id&operator'
            );

            if(is_array($contributions)){
                $contributions[rgar($entry,'id')] = $submission;
            }else{
                $contributions = array(rgar($entry,'id') => $submission);
            }
            update_post_meta($campaign_id,'fs_contributions',$contributions);
        }

        // frontend create campaign forms submission
        function fs_create_campaign_form_submission($entry, $form){

            // check if the create campaign form was submitted
            if(rgar($entry, 'form_id') != $this->fs_settings['micro_campaign_form_id']){return;}

            // build campaign details
            $parent_id = sanitize_text_field(rgar($entry, '10'));
            $title = sanitize_text_field(rgar($entry, '1'));
            $description = esc_textarea(rgar($entry, '2'));

            $name = sanitize_text_field(rgar($entry, '8.3')) . ' ' . sanitize_text_field(rgar($entry, '8.6'));
            $email = sanitize_text_field(rgar($entry, '9'));

            $start = rgar($entry, '4') . ' ' . rgar($entry, '6');
            $formatted_start = date('Y-m-d H:i', strtotime($start));            

            $end = rgar($entry, '5') . ' ' . rgar($entry, '7');
            $formatted_end = date('Y-m-d H:i', strtotime($end));

            $goal = sanitize_text_field(rgar($entry, '3'));

            // make the micro campaign
            $post_args = array(
                'post_type' => 'fs_micro_campaign',
                'post_title'    => $title,
                'post_content'  => $description,
                'post_status'   => 'pending',
            );
             
            // Insert the post into the database
            $post_id = wp_insert_post($post_args);

            // update campaign meta
            if($post_id){
                update_post_meta($post_id, 'fs_parent_campaign', $parent_id);
                update_post_meta($post_id, 'fs_campaign_start', $formatted_start);
                update_post_meta($post_id, 'fs_campaign_end', $formatted_end);
                update_post_meta($post_id, 'fs_campaign_goal', $goal);
                update_post_meta($post_id, 'fs_campaign_creator_name', $name);
                update_post_meta($post_id, 'fs_campaign_creator_email', $email);
            }
        }        



        //////////////////////////////////
        /////////// SHORTCODES ///////////
        //////////////////////////////////

        // shortcode [fs-campaign-list]
        function fs_campaign_list($atts){
            $args = array(
                'post_type' => 'fs_campaign',
                'posts_per_page' => -1,
                'post_status' => 'published',
                'orderby' => 'meta_value',
                'meta_key' => 'fs_campaign_end',
                'order' => 'ASC'
            );
            $now = date('Y-m-d H:i');
            if($atts['view'] == 'current'){
                $args['meta_query'] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'fs_campaign_start',
                        'value'   => $now,
                        'compare' => '<'
                    ),                   
                    array(
                        'key'     => 'fs_campaign_end',
                        'value'   => $now,
                        'compare' => '>'
                    )
                );
                $title = 'Current Campaigns';
            }elseif($atts['view'] == 'upcoming'){
                $args['meta_query'] = array(
                    array(
                        'key'     => 'fs_campaign_start',
                        'value'   => $now,
                        'compare' => '>'
                    )
                );
                $title = 'Upcoming Campaigns';
            }elseif($atts['view'] == 'past'){
                $args['meta_query'] = array(
                    array(
                        'key'     => 'fs_campaign_end',
                        'value'   => $now,
                        'compare' => '<'
                    )
                );
                $title = 'Past Campaigns';
            }else{
                $args = false;
            }
            ob_start();
            ?>
            <?php $campaigns = new WP_Query($args); ?>
            <?php if($campaigns->have_posts()) : ?>
                <div class="fs-campaign-shortcode">
                    <h3 class="fs-campaign-list-title"><?php echo $title; ?></h3>
                    <ul class="fs-campaign-list">
                        <?php while($campaigns->have_posts()) : $campaigns->the_post(); ?>
                        <li>
                            <?php $has_thumb = false; ?>
                            <?php if(has_post_thumbnail()) : ?>
                                <div class="fs-campaign-thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>
                                <?php $has_thumb = true; ?>
                            <?php endif; ?>
                            <div class="fs-campaign-info <?php if($has_thumb){echo 'has-thumb';}?>">
                                <h4 class="fs-campaign-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h4>
                                <ul>
                                    <li><strong><?php _e('Start Date:','fs'); ?></strong> <?php echo date(get_option('date_format'),strtotime(get_post_meta(get_the_ID(),'fs_campaign_start',true))); ?></li>
                                    <li><strong><?php _e('End Date:','fs'); ?></strong> <?php echo date(get_option('date_format'),strtotime(get_post_meta(get_the_ID(),'fs_campaign_end',true))); ?></li>
                                    <li><strong><?php _e('Campaign Goal:','fs'); ?></strong> $<?php echo get_post_meta(get_the_ID(),'fs_campaign_goal',true); ?></li>
                                </ul>
                                <?php the_excerpt(); ?> 
                                <span class="fs-clearer"></span>
                            </div>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php
            return ob_get_clean();
        }

        // shortcode [fs-micro-campaign-form]
        function fs_micro_campaign_form($atts){
            ob_start();
            ?>

            <?php // must be logged in to submit ?>
            <?php if(is_user_logged_in()) : ?>

                <?php // must have parent campaign in $_GET data ?>
                <?php if(isset($_GET['campaign']) && !empty($_GET['campaign'])) : ?>

                    <?php $parent_id = sanitize_text_field($_GET['campaign']); ?>
                    <h3 class="fs-parent-campaign-details-title">Parent Campaign Details</h3>
                    <ul class="fs-parent-campaign-details">
                        <li><strong>Title:</strong> <?php echo get_the_title($parent_id); ?></li>
                        <li><strong>Start:</strong> <?php echo date('m/d/Y @ g:i a', strtotime(get_post_meta($parent_id, 'fs_campaign_start', true))); ?></li>
                        <li><strong>End:</strong> <?php echo date('m/d/Y @ g:i a', strtotime(get_post_meta($parent_id, 'fs_campaign_end', true))); ?></li>
                        <li><strong>Goal:</strong> $<?php echo number_format(get_post_meta($parent_id, 'fs_campaign_goal', true)); ?></li>
                    </ul>
                    <?php echo do_shortcode('[gravityform id="'.$this->fs_settings['micro_campaign_form_id'].'" title="false" description="false" ajax="true"]'); ?>
                <?php else : ?>
                   
                    <?php if($campaigns = $this->fs_get_campaigns()) : ?>
                        <form method="GET" action="">
                            <p>
                                <label>Please select the Parent Campaign that your Micro Campaign will support.</label>
                                <br/>
                                <select name="campaign">
                                    <option value=""> - Choose One - </option>
                                    <?php foreach($campaigns as $campaign) : ?>
                                    <option value="<?php echo $campaign->ID; ?>"><?php echo $campaign->post_title; ?></option>
                                    <?php endforeach; ?>
                                </select>      
                            </p>
                            <p>
                                <input class="button" type="submit" value="Next" />
                            </p>
                        </form>
                    <?php else : ?>
                        <p>Sorry, there are currently no Campaigns to support.</p>
                    <?php endif; ?>
              
                <?php endif; ?>
            <?php else : ?>
                <p>You must be loggeed in to create a Micro Campaign. <a href="/wp-login.php">Log in here</a>.</p>
            <?php endif; ?>
                        
            <?php 
            return ob_get_clean();
        }



        ////////////////////////////////////////
        //////// FUNDING SERIOUS GETTERS ///////
        ////////////////////////////////////////

        // get micro campaigns
        function fs_get_micro_campaigns($post_id){
            $args = array(
                'post_type' => 'fs_micro_campaign',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'fs_parent_campaign',
                        'value' => $post_id
                    )
                )
            );
            $campaigns = new WP_Query($args);
            return $campaigns->posts;
        }

        // get campaign meta fields
        function fs_get_number_of_contributions($post_id){
            $total = 0;
            $contributions = get_post_meta($post_id,'fs_contributions',true);
            if($contributions){
                $total = count($contributions);  
            }
            return $total;
        }

        function fs_get_amount_contributed($post_id){
            $total = 0;
            $other = get_post_meta($post_id,'fs_other_contributions',true);
            $contributions = get_post_meta($post_id,'fs_contributions',true);
            if($contributions){
                foreach($contributions as $contribution){
                    $total += $contribution['amount'];
                }
            }

            return $other ? $total + $other : $total;
        }

        function fs_get_campaign_days_left($post_id){
            $now = time();
            $end = get_post_meta($post_id, 'fs_campaign_end', true);
            $days_left = strtotime($end) - $now;
            if($days_left < 0){$days_left = 0;}
            return floor($days_left/(60*60*24));
        }        

        function fs_get_campaign_days_until($post_id){
            $now = time();
            $start = get_post_meta($post_id, 'fs_campaign_start', true);
            $days_until = strtotime($start) - $now;
            return floor($days_until/(60*60*24));
        }

        function fs_is_campaign_upcoming($post_id){
            $now = time();
            $start = strtotime(get_post_meta($post_id, 'fs_campaign_start', true));
            if($start > $now){return true;}
            else{return false;}
        }        

        function fs_has_campaign_ended($post_id){
            $now = time();
            $end = strtotime(get_post_meta($post_id, 'fs_campaign_end', true));
            if($end < $now){return true;}
            else{return false;}
        }

        function fs_get_campaigns(){
            $args = array(
                'post_type' => 'fs_campaign',
                'posts_per_page' => -1,
                'post_status' => array('publish','draft')
            );
            $campaigns = new WP_Query($args);
            return $campaigns->posts;
        }             

        function fs_get_all_campaigns(){
            $args = array(
                'post_type' => array('fs_campaign', 'fs_micro_campaign'),
                'posts_per_page' => -1,
                'post_status' => array('publish','draft')
            );
            $campaigns = new WP_Query($args);
            return $campaigns->posts;
        }     

        // helpers
        function fs_debug_log($msg, $title = ''){
            $error_dir = dirname(__file__).'/debug.log';
            $date = date('d.m.Y h:i:s');
            $msg = print_r($msg, true);
            $log = $title . "  |  " . $msg . "\n";
            error_log($log, 3, $error_dir);
        }

        function fs_get_path($file){
            return trailingslashit(dirname($file));
        }
        
        function fs_get_dir($file){
            $dir = trailingslashit(dirname($file));
            $count = 0;
            
            // sanitize for Win32 installs
            $dir = str_replace('\\' ,'/', $dir); 
            
            // if file is in plugins folder
            $wp_plugin_dir = str_replace('\\' ,'/', WP_PLUGIN_DIR); 
            $dir = str_replace($wp_plugin_dir, plugins_url(), $dir, $count);
            
            if($count < 1){
                // if file is in wp-content folder
                $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
                $dir = str_replace($wp_content_dir, content_url(), $dir, $count);
            }
           
            if($count < 1){
                // if file is in ??? folder
                $wp_dir = str_replace('\\' ,'/', ABSPATH); 
                $dir = str_replace($wp_dir, site_url('/'), $dir);
            }
            
            return $dir;
        }

    }
    $fs = new FundingSerious();



    ///////////////////////////
    //////// widget /////////// 
    ///////////////////////////

    class FsCampaignList extends WP_Widget {
 
        public function __construct() {
            parent::__construct(
                'fs_campaign_list',
                __('Funding Serious', 'fs'),
                array(
                    'classname'   => 'fs_campaign_list_widget',
                    'description' => __( '', 'fs')
                )
            );
        }
     
        public function widget($args, $instance){    
            global $fs;
            echo $fs->fs_campaign_list($args);
        } 
    }

?>